use_bpm 120

# unique notes
# :f3
# :g3
# :a3
# :as3
# :c4
# :d4
# :c2

define :bruder_jakob do
  2.times do
    play :f3
    sleep 1
    play :g3
    sleep 1
    play :a3
    sleep 1
    play :f3
    sleep 1
  end
  
  2.times do
    play :a3
    sleep 1
    play :as3
    sleep 1
    play :c4
    sleep 2
  end
  
  2.times do
    play :c4
    sleep 0.5
    play :d4
    sleep 0.5
    play :c4
    sleep 0.5
    play :as3
    sleep 0.5
    play :a3
    sleep 1
    play :f3
    sleep 1
  end
  
  2.times do
    play :g3
    sleep 1
    play :c2
    sleep 1
    play :f3
    sleep 2
  end
end

live_loop :foo do
  use_synth :saw
  bruder_jakob
end
