use_bpm 120

define :linearMelody do
  delta = -0.001
  minimum = 0.005
  maximum = 0.1
  value = maximum
  
  loop do
    value = value + delta
    
    if(value <= 0)
      value = minimum
    end
    
    # invert direction if we hit minimum or maximum
    if (value <= minimum or value >= maximum)
      delta = delta * -1.0
    end
    
    sample :tabla_ke1
    sleep value
  end
end

define :smallestPossibleSleepTime do
  return 0.005
end

define :linearMelody2 do |targetValue, startValue, delta = 0.001|
  if delta == 0.0
    return targetValue
  end
  
  # lower values mess up thread synchronisation
  if targetValue <= smallestPossibleSleepTime
    return smallestPossibleSleepTime
  end
  
  if targetValue == startValue
    return targetValue
  end
  
  delta = delta.abs
  if targetValue < startValue
    delta = delta * -1.0
  end
  
  loop do
    startValue = startValue + delta
    
    if((delta < 0 and startValue <= targetValue) or (delta > 0 and startValue >= targetValue))
      return targetValue
    end
    
    sample :tabla_ke1
    sleep startValue
  end
end

define :targetsAndDeltasRandom do |size, minimumTarget, maximumTarget, minimumDelta, maximumDelta|
  targetValues = []
  deltas = []
  
  size.times do
    targetValues = targetValues + [rrand(minimumTarget, maximumTarget)]
    deltas = deltas + [rrand(minimumDelta, maximumDelta)]
  end
  
  return targetValues.ring, deltas.ring
end

define :playRandom do
  size = 10
  minimumTarget = 0.006
  maximumTarget = 0.009
  minimumDelta = 0.0001
  maximumDelta = 0.0008
  startValue = 0.1
  endValue = startValue
  
  targetValues, deltas = targetsAndDeltasRandom size, minimumTarget, maximumTarget, minimumDelta, maximumDelta
  
  puts ("targetValues: " + targetValues.to_s)
  puts ("deltas: " + deltas.to_s)
  
  tick_reset(:index)
  100.times do
    i = tick(:index)
    targetValue = targetValues[i]
    delta = deltas[i]
    
    puts i
    puts ("from: " + startValue.to_s + ", to: " + targetValue.to_s + ", delta: " + delta.to_s)
    
    linearMelody2 targetValue, startValue, delta
    
    startValue = targetValue
  end
  
  linearMelody2 endValue, startValue, maximumDelta
  
end

define :targetsAndDeltasFixedInterval do |size, maximumTarget| #, minimumDelta
  targetValues = []
  deltas = []
  
  if size <= 0
    return targetValues.ring, deltas.ring
  end
  
  if size == 1
    return [smallestPossibleSleepTime].ring, [minimumDelta].ring
  end
  
  maximumRange = maximumTarget - smallestPossibleSleepTime
  interval = maximumRange / (size - 1)
  stepsPerInterval = 30.0
  delta = 0.001 #interval / stepsPerInterval
  
  puts "max range: " + maximumRange.to_s + ", interval: " + interval.to_s + ", steps per interval: " + stepsPerInterval.to_s + ", delta: " + delta.to_s
  
  tick_reset(:index)
  size.times do
    i = tick(:index)
    
    targetValue = 0
    
    if i % 2 == 0
      targetValue = smallestPossibleSleepTime + interval * (i+1)/2
    else
      targetValue = maximumTarget - interval * (i+1)/2
    end
    
    targetValues = targetValues + [targetValue]
    deltas = deltas + [delta]
  end
  
  return targetValues.ring, deltas.ring
end

define :playFixed do
  size = 10
  maximumTarget = 0.01
  #minimumDelta = 0.01
  startValue = 0.1
  endValue = startValue
  
  targetValues, deltas = targetsAndDeltasFixedInterval size, maximumTarget   #, minimumDelta
  
  puts ("targetValues: " + targetValues.to_s)
  puts ("deltas: " + deltas.to_s)
  
  tick_reset(:index)
  100.times do
    i = tick(:index)
    targetValue = targetValues[i]
    delta = deltas[i]
    
    puts i
    puts ("from: " + startValue.to_s + ", to: " + targetValue.to_s + ", delta: " + delta.to_s)
    
    linearMelody2 targetValue, startValue, delta
    
    startValue = targetValue
  end
  
  linearMelody2 endValue, startValue, delta
  
end

define :targetsAndDeltasConstant do
  targetValues = [0.05, 0.008, 0.04, 0.007, 0.07, 0.01, 0.06, 0.008]
  deltas = []
  
  tick_reset(:index)
  (targetValues.length - 1).times do
    i = tick(:index)
    nextI = 0
    if i == targetValues.length
      nextI = 0
    else
      nextI = i + 1
    end
    
    
    interval = (targetValues[i] - targetValues[nextI]).abs
    delta = interval / 30
    puts "interval: " + interval.to_s + ", delta: " + delta.to_s
    deltas = deltas + [delta]
  end
  
  return targetValues.ring, deltas.ring
end

define :playConstant do
  targetValues, deltas = targetsAndDeltasConstant
  
  puts ("targetValues: " + targetValues.to_s)
  puts ("deltas: " + deltas.to_s)
  
  tick_reset(:index)
  100.times do
    i = tick(:index)
    startValue = targetValues[i]
    targetValue = targetValues[i + 1]
    delta = deltas[i]
    
    puts i
    puts ("from: " + startValue.to_s + ", to: " + targetValue.to_s + ", delta: " + delta.to_s)
    
    linearMelody2 targetValue, startValue, delta
  end
end

define :playConstant2 do
  loop do
    sample :tabla_ke1, amp: 0.5
    sleep 0.005
  end
end

define :notes_hz do |n|
  puts n.to_s + ", hz: " + (midi_to_hz(n)).to_s
end

notes_hz :e3
notes_hz :f3
notes_hz :fs3
notes_hz :g3
notes_hz :gs3
notes_hz :a3
notes_hz :as3
notes_hz :b3
notes_hz :c3
notes_hz :cs3
notes_hz :d3
notes_hz :ds3
notes_hz :e4

define :intro do
  first_note = note_to_sleep :f2
  linearMelody2 first_note, 0.5, 0.01
end

define :outro do
  last_note = note_to_sleep :f2
  linearMelody2 0.07, last_note, 0.001
  linearMelody2 0.1, 0.07, 0.007
  linearMelody2 0.5, 0.1, 0.009
end

define :bruder_jakob_sleep do
  s_f2 = note_to_sleep :f2
  s_g2 = note_to_sleep :g2
  s_a2 = note_to_sleep :a2
  s_as2 = note_to_sleep :as2
  s_c3 = note_to_sleep :c3
  s_d3 = note_to_sleep :d3
  s_c1 = note_to_sleep :c1
  
  2.times do
    hold_sleep s_f2, 1
    hold_sleep s_g2, 1
    hold_sleep s_a2, 1
    hold_sleep s_f2, 1
  end
  
  2.times do
    hold_sleep s_a2, 1
    hold_sleep s_as2, 1
    hold_sleep s_c3, 2
  end
  
  2.times do
    hold_sleep s_c3, 0.5
    hold_sleep s_d3, 0.5
    hold_sleep s_c3, 0.5
    hold_sleep s_as2, 0.5
    hold_sleep s_a2, 1
    hold_sleep s_f2, 1
  end
  
  use_synth :saw
  
  play :g2
  sleep 1
  play :c1
  sleep 1
  play :f2
  sleep 2
  
  play :g2
  sleep 1
  play :c1
  sleep 1
  hold_sleep s_f2, 2
  
end

live_loop :sampleSound do
  intro
  bruder_jakob_sleep
  outro
  stop
end

define :my_loop do
  use_synth :saw
  #loop do
  play :f3 , sustain: 50
  sleep 50
  #end
end

#live_loop :targetSound do
#  my_loop
#end

# rt_sleep is sleep duration in real time seconds.
# rt_sleep is independent of bpm.
# length is the duration of the note (whole note = 1, half note = 0.5, etc)
# length depends on bpm
define :hold_sleep do |rt_sleep, length|
  bt_length = bt length
  number_of_sleeps = (bt_length / rt_sleep).floor
  number_of_sleeps.times do
    sample :tabla_ke1
    sleep rt_sleep
  end
end

define :play_sleep do |n, length|
  rt_sleep = note_to_sleep n
  hold_sleep rt_sleep, length
end

define :note_to_sleep do |n|
  return g3_sleep * (g3_interval n)
end

# n is a note name, like :a3
define :hold_note do |n, length|
  
end

define :g3_interval do |n|
  g3hz = midi_to_hz(:g3)
  nhz = midi_to_hz(n)
  return g3hz / nhz
end

define :f3_sleep do
  #return g3_sleep * (g3_interval :f3)
  return note_to_sleep :f3
end

define :fs3_sleep do
  #return g3_sleep * (g3_interval :fs3)
  return note_to_sleep :fs3
end

define :g3_sleep do
  return rt 0.010155
end

define :gs3_sleep do
  #return g3_sleep * (g3_interval :gs3)
  return note_to_sleep :gs3
  #return 0.005379423861618974
end

puts "half step ratio: " + (half_step_ratio).to_s
puts (f3_sleep).to_s
puts (fs3_sleep).to_s
puts (g3_sleep).to_s
puts (gs3_sleep).to_s

#in_thread(name: :looper) do
#my_loop
#end

#playConstant2



# playFixed

# playRandom

# linearMelody2 0.01, 0.1, 0.001